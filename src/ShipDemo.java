/**
 * Created by Dmitry Vereykin on 7/23/2015.
 */
public class ShipDemo {
    public static void main(String[] args) {
        Ship[] ships = new Ship[3];

        ships[0] = new Ship("Yamato", "1940");
        ships[1] = new CruiseShip("Allure of the Seas", "2010", 6296);
        ships[2] = new CargoShip("Barzan", "2015", 35, 199744);

        for (int n = 0; n < ships.length; n++) {
            System.out.println(ships[n].toString());
        }

        System.out.println("\nInformation is taken from Wiki.");
    }
}
