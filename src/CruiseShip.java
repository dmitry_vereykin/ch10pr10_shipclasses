/**
 * Created by Dmitry Vereykin on 7/23/2015.
 */
public class CruiseShip extends Ship {
    int maxPassengers;

    public CruiseShip() {
        this.maxPassengers = 0;
    }

    public CruiseShip(String name, String year,int maxPassengers) {
        super(name, year);
        this.maxPassengers = maxPassengers;
    }

    public void setMaxPassengers(int maxPassengers) {
        this.maxPassengers = maxPassengers;
    }

    public int getMaxPassengers() {
        return maxPassengers;
    }

    public String toString() {
        String str = "\nMaximum number of passengers: " + this.getMaxPassengers();
        return super.toString() + str;
    }
}
